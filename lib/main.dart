import 'package:location/location.dart';
import 'package:flutter/material.dart';
import 'package:women_saftey/SIGNIN/signup.dart';
import 'package:women_saftey/login.dart';
import 'package:firebase_core/firebase_core.dart';

import 'home.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();

  runApp( MaterialApp(
    home: Login(),
  ));
}


