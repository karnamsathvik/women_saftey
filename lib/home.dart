import 'package:flutter/material.dart';
import 'package:location/location.dart';

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blueGrey[900],
        // backgroundColor: Colors.amberAccent,
        //foregroundColor: Colors.purpleAccent,
        centerTitle: true,
        title:Text('BeSafe'),
      ),
      body: Column(
        children: [
          SizedBox(height: 20,),
          Row(
            children: [
              SizedBox(width: 10,),
              Expanded(child: ClipRRect(
                  borderRadius: BorderRadius.circular(10.0),
                  child: Container(
                      color: Colors.purple[200],
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Icon(Icons.add_alarm,size: 30,),
                      ))
              )),
              SizedBox(width: 5,),
              Expanded(child: ClipRRect(
                  borderRadius: BorderRadius.circular(10.0),
                  child: Container(
                      color: Colors.blue[100],
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Icon(Icons.car_rental,size: 30,),
                      ))
              )),
              SizedBox(width: 10,)
            ],
          ),
          SizedBox(height: 30,),
          Expanded(child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: ClipRRect(
                borderRadius: BorderRadius.circular(10.0),
                child: Container(
                    color: Colors.red,
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Center(child: Text("SOS",style: TextStyle(fontSize: 40,fontWeight: FontWeight.bold),)),
                    ))
            ),
          )),
          SizedBox(height: 30,),
          Row(
            children: [
              SizedBox(width: 10,),
              Expanded(child: ClipRRect(
                  borderRadius: BorderRadius.circular(10.0),
                  child: Container(
                      color: Colors.greenAccent,
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Icon(Icons.phone,size: 30,),
                      ))
              )),
              SizedBox(width: 5,),
              Expanded(child: ClipRRect(
                  borderRadius: BorderRadius.circular(10.0),
                  child: Container(
                      color: Colors.amber[100],
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Icon(Icons.location_on,size: 30,),
                      ))
              )),
              SizedBox(width: 10,)
            ],
          ),
          SizedBox(height: 20,)
        ],
      ),
    );
  }

  Future<void> func() async {
    var location = Location();

    bool _serviceEnabled;
    PermissionStatus _permissionGranted;

    _serviceEnabled = await location.serviceEnabled();
    if (!_serviceEnabled) {
      _serviceEnabled = await location.requestService();
      if (!_serviceEnabled) {
        return;
      }
    }
    _permissionGranted = await location.hasPermission();
    if (_permissionGranted == PermissionStatus.denied) {
      _permissionGranted = await location.requestPermission();
      if (_permissionGranted != PermissionStatus.granted) {
        return;
      }
    }
  }

  @override
  void initState() {
    func();
  }
}